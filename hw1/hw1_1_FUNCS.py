import numpy as np

#Part 1
def coordMin_2D(twoD):
	"""returns x,y indices (tuple) of min val in 2D numpy array """
	iy = indMin_2D(twoD, 0)
	ix = indMin_2D(twoD, 1)
	return ix,iy
	
def indMin_2D(twoD, axis):
	"""returns index of minimum in y,x (axis = 0,1) direction
	
	Defaults: -- if multiple indicies, floating pt avg index returned
	
	"""
	oneD = np.min(twoD, axis = axis)
	i1st = np.argmin(oneD)  #1st index meets cond
	mask = (oneD == oneD[i1st])
	inds = np.argwhere(mask == True)
	return np.float( np.sum(inds) ) /np.size(inds)

#Part 2

#Part 3
def maskMax_2D(twoD):
	"""returns 2D mask True where Red (max value) letters are

		axis = 0 -- along vertical
	"""
	oneD = np.max(twoD, axis = axis)
	i1st = np.argmax(oneD)  #1st index meets cond
	return oneD == oneD[i1st]