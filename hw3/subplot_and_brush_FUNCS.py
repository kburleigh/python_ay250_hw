"""Supporting funcs for "subplot_and_brush.py"

Usage: in subplot_and_brush.py, do...
        -> import subplot_and_brush_FUNCS as my

"""
from matplotlib import pyplot as plt
import numpy as np


def rePlot(clickAxis, xRng, yRng,
            fig, ax, 
            dtypFlow,data,myAxes,myScat,
            nRowCol, myCol):
    """erases axes and re-plots scatter plot. Preserves coloring for data
    in xRng,yRng. Sets color = "0.9" (faded gray) for all other data.
    
    Usage: 
    clickAxis, xRng, yRng -- from RectInteract()
        -- user defined rectangle with cursor
    Other args -- from main plotting call in "subplot_and_brush.py"
     
    """
    print "Replotting a box with: xRng, yRng =", xRng, yRng

    ind = -1
    for i,axes in enumerate(myAxes):
        if (clickAxis == axes): 
            ind = i
    if ind == -1: raise ValueError("ind = -1, clickAx not in ax??")

    clickScat = myScat[ind]
    xy = clickScat.get_offsets()
    xdata = xy[:,0]
    ydata = xy[:,1]

    #indices for data in xRng,yRng, and otherwise
    indGood = indGoodXY(xdata, ydata, xRng, yRng)
    indBad = indBadXY(xdata, ydata, xRng, yRng)

    #### LARGE PLOTTING CALL (see plotting call in main func)
    for col in range(nRowCol):
        for row in range(nRowCol):
            myax = ax[row, col]      
            myax.clear()  #clear axis before redraw!

            newCol = myCol[indGood]        

            xName = dtypFlow["names"][col]
            yName = dtypFlow["names"][row]
            x = data[xName]
            y = data[yName]
           
            myscat = myax.scatter(x[indGood],y[indGood], s=20, c=newCol)
            myscat = myax.scatter(x[indBad],y[indBad], s=20, c="0.9") #fade

            #make subplots look better
            myax.set_xlim([x.min(),x.max()])
            myax.set_ylim([y.min(),y.max()])
            if xName == yName:
                myax.set_label( xName )
                myax.legend(loc=2)

            plt.setp(myax.get_xticklabels(), visible=False)  
            plt.setp(myax.get_yticklabels(), visible=False)
            myax.set_xlabel("",visible=False)  # title of axis
            myax.set_ylabel("",visible=False)
            if col == 0: 
                plt.setp( myax.get_yticklabels(), visible=True)
                myax.set_ylabel( dtypFlow["names"][row], visible=True)
            if row == nRowCol-1: 
                plt.setp( myax.get_xticklabels(), visible=True)
                myax.set_xlabel( dtypFlow["names"][col], visible=True)
    fig.canvas.draw()
    #########################


def x_or_y_Good(data, rng):
    """for x or y data, return where data is INSIDE rng"""
    ind1 = data >= rng[0]
    ind1 = np.where(ind1 == True)[0]
    ind2 = data <= rng[1]
    ind2 = np.where(ind2 == True)[0]
    s1 = set(ind1)
    s2 = set(ind2)
    s3 = set.intersection(s1,s2)
    return list(s3)

def x_or_y_Bad(data, rng):
    """for x or y data, return where data is OUTSIDE rng"""
    ind1 = data < rng[0]
    ind1 = np.where(ind1 == True)[0]
    ind2 = data > rng[1]
    ind2 = np.where(ind2 == True)[0]
    s1 = set(ind1)
    s2 = set(ind2)
    s3 = set.union(s1,s2)
    return list(s3)

def indGoodXY(xdata, ydata, xRng, yRng):
    """return list of indices where BOTH x,y data INSIDE rng"""
    xind = x_or_y_Good(xdata, xRng)
    yind = x_or_y_Good(ydata, yRng)
    sx = set(xind)
    sy = set(yind)
    sF = set.intersection(sx,sy)
    return list(sF)

def indBadXY(xdata, ydata, xRng, yRng):
    """return list of indices where BOTH x,y data OUTSIDE rng"""
    xind = x_or_y_Bad(xdata, xRng)
    yind = x_or_y_Bad(ydata, yRng)
    sx = set(xind)
    sy = set(yind)
    sF = set.union(sx,sy)
    return list(sF)
