"""Brushing code, python ay250, hw3
"""

from matplotlib import pyplot as plt
import matplotlib.patches
import numpy as np

class RectInteract:
    def __init__(self, figure,axis):
        self.fig = figure
        self.ax = axis
        self.rect = matplotlib.patches.Rectangle( (0,0), 0.0, 0.0, fill=True, alpha=0.5, color = "0.5")
        self.cid = self.fig.canvas.mpl_connect('button_press_event', self.onClick)
        self.rid = self.fig.canvas.mpl_connect('button_release_event', self.onRel)

        self.ax.add_patch(self.rect)
        kid = self.fig.canvas.mpl_connect('key_press_event', self.onKey)

    def onClick(self, event):
        print '%s, Event: ' % "Click", event
        
        if event.inaxes != self.ax: return
        
        self.ix = event.x
        self.iy = event.y
        self.ixdata = event.xdata
        self.iydata = event.ydata
        mid = self.fig.canvas.mpl_connect('motion_notify_event', self.onMove)
    
    def onMove(self, event):
        print '%s, Event: ' % "Move", event

        if event.inaxes != self.ax: return

        self.dx = event.xdata - self.ixdata
        self.dy = event.ydata - self.iydata

        self.rect.set_xy((self.ixdata,self.iydata))  #updated way do it from notebook
        self.rect.set_width(self.dx)
        self.rect.set_height(self.dy)
        self.fig.canvas.draw()

    def onRel(self, event):
        print '%s, Event: ' % "Release", event

        if event.inaxes != self.ax: return

        self.dx = event.xdata - self.ixdata
        self.dy = event.ydata - self.iydata

        self.rect.set_xy((self.ixdata,self.iydata))  #updated way do it from notebook
        self.rect.set_width(self.dx)
        self.rect.set_height(self.dy)
        self.fig.canvas.draw()

    def onKey(self, event):
        print '%s, Event: ' % "Key", event
        sys.stdout.flush()
        key = event.key
        if key == "d":
            print "key is d"
        if key == "q":
            print "key is q"
        else:
            print "key is %s, which does nothing" % key

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_title('click to build line segments')
x = np.linspace(1,5)
ax.plot(x,x**2)  # empty line
RectMy = RectInteract(fig,ax)

plt.show()