"""testing for python book chapters"""

		
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.axes
import matplotlib.cm as cm
import matplotlib.patches 
    		
class DrawRect:
	"""Calling this class with rectangle object as argument
	allows that rect to Handle Events"""
	
	def __init__(self, figure, axis):
		self.fig = figure
		self.ax = axis	  #list of axis obj, one for each subplot 
		self.rect = matplotlib.patches.Rectangle( (0,0), 0.5, 0.5, fill=True, alpha=0.5, color = "0.5")

		self.ix = None #Initial click location, on Frame
		self.iy = None
		self.ixdata = None #Initial click location, on specific subplot
		self.iydata = None
		self.dx = None 	#width,length rectange from ix,iy
		self.dy = None
		
		print self.rect
		print type(self.rect)
		self.ax.add_patch(self.rect) 
		self.fig.canvas.draw()

		self.cidClick = self.fig.canvas.mpl_connect(
            'button_press_event', self.onClick)
# 		self.cidLetGo = self.fig.canvas.mpl_connect(
#             'button_release_event', self.onLetGo) 

	def disconnect(self):
		"""disconnect all the stored connection ids"""
		self.fig.canvas.mpl_disconnect(self.cidClick)
		self.fig.canvas.mpl_disconnect(self.cidMove)
		self.fig.canvas.mpl_disconnect(self.cidLetGo)
		self.fig.canvas.mpl_disconnect(self.cidKeyBoard)


	def onClick(self, event):
		"""Clicking enables "motion" and "button release" event handling"""
		print "Event type: %s" % event.name
		print "type(xdata): ", type(event.xdata)
		print "x,y,xdata,ydata = %f %f %f %f" % (event.x,event.y,event.xdata,event.ydata)
		
		# self.didClick = True
# 		self.didRel = False

		# if event.inaxes not in self.ax: 
		if event.inaxes != self.ax: 
			return
		self.ix = event.x
		self.iy = event.y
		self.ixdata = event.xdata
		self.iydata = event.ydata
			
		#draw rect
		print self.rect
		print type(self.rect)
		#self.rect.remove()
		self.ax.patch.set_xy((0.4,0.4))  #updated way do it from notebook
		self.ax.patch.set_width(0.3)
		self.ax.patch.set_height(0.3)
		self.ax.patch.set_color("blue")
		self.fig.canvas.draw()
		
		# self.rect.set_xy((self.ix,self.iy))
# 		self.rect.set_width(self.dx)
# 		self.rect.set_height(self.dy)
# 		self.fig.canvas.draw()	
	
# 		self.cidMove = self.fig.canvas.mpl_connect(
# 			'motion_notify_event', self.onMove)
		#self.enableMoveRelease()

	def onLetGo(self, event):
		"""on click release, give option to delete draw rectangle"""
		print "Event type: %s" % event.name
# 		print "self-> x,y,xdata,ydata,dx,dy = %f %f %f %f %f %f" % (self.ix,self.iy,self.ixdata,self.iydata,self.dx,self.dy)

		# self.didClick = False
# 		self.didRel = True
		
		self.cidKeyBoard = self.fig.canvas.mpl_connect('key_press_event', self.onKeyBoard) 
		printFinalOpts()
		#self.enableKey()

	def onMove(self, event):
		"""during motion redraw rectangle to where mouse is"""
		# #If did not click, do not register motion 
# 		if self.didClick is not True:  
# 			return
# 		#If released click, do not register motion
# 		if self.didRel is True:
# 			return
		#ignore if move out of axis for plot
		# if event.inaxes not in self.ax: 
		if event.inaxes != self.ax: 
			return
		
		self.dx = event.xdata - self.ixdata
		self.dy = event.ydata - self.iydata

		print "Event type: %s" % event.name
		print "dx, dy, ix,iy,ixdata,iydata = %f %f %f %f %f %f" % (self.dx, self.dy, self.ix, self.iy, self.ixdata, self.iydata)
		
		#redraw rect by removing previous rect, and drawing new one
		#self.rect.remove()
		self.ax.patch.set_xy((self.ix,self.iy))  #updated way do it from notebook
		self.ax.patch.set_width(self.dx)
		self.ax.patch.set_height(self.dy)
		self.ax.patch.set_color("0.5")
		self.fig.canvas.draw()

	
	def onKeyBoard(self, event):
		#If did not release click, do not register Keys
		# if self.didRel is not True:
# 			return		

		print "Event type: %s" % event.name
		print "self-> x,y,xdata,ydata,dx,dy = %f %f %f %f %f %f" % (self.ix,self.iy,self.ixdata,self.iydata,self.dx,self.dy)

		if event.key == "d":
			self.rect.remove()
			self.fig.canvas.draw()
		elif event.key == "q":
			self.disconnect()  #stop event handling
		else:
			print "that key does nothing!"
			return
	
def printFinalOpts():
	one = "q -- stop event handling" 
	two = "d -- delete last drawn rectangle"
	other = "other -- does nothing"
	print "Key options:"
	print "%s\n%s\n%s" % (one,two,other)
		
fig= plt.figure()
ax = fig.add_subplot(1,1,1)
x = np.random.rand(20)
ax.scatter(x,x**2)

Drect = DrawRect(fig,ax)
plt.show()