"""Brushing Code -- python ay250, hw3, Problem 3

USAGE: 
Make a plot with multiple subplots each being a scatter plot, call plt.show(), then call RectInteract() object. 
    
User interactively draws rectangle(s) with mouse, region defined
is used as filter, subplots are redrawn with data outside region
faded out


RUN  (command line only!): 
python subplot_and_brush.py <pathToDataFile> <dataFile>

Acceptable DATA FILES:
-- only "flowers.csv"  (right now)

DRAW RECTANGLES -- Warning!
1) click a subplot
2) holding mouse down, move cursor from BOTTOM LEFT to TOP RIGHT
    a) DO NOT move cursor RIGHT to LEFT -- will not identify points
    b) DO NOT move cursor outside a subplot -- will not identify points
3) release mouse -- All but selected points fade out
4) click mouse -- rectangle goes away...

NOT INCLUDED:
-- not general (only written to work with "flowers.csv")
-- key_press_event -- does not yet work

"""

import argparse
from matplotlib import pyplot as plt
import matplotlib.patches
import numpy as np
import subplot_and_brush_FUNCS as my #support funcs


class RectInteract(object):
    """Usage:
    Make a plot with multiple subplots each of a scatter plot, 
    call plt.show(), then call this object. 
    User interactively draws rectangle(s) with mouse, region defined
    is used as filter, subplots are redrawn with data outside region
    faded out
    
    Args:
    figure,axis -- defined in making subplot
    dtypFlow,data -- dtype descripter and data used to read in data from
                     text file
    myAxes,myScat -- list of axes and ax.scatter() objects for subplots
    nRowCol -- # subplots in vertical direction (which must equal # 
                in horizontal)
    myCol -- list of rgb vals of same length of each data colm use to 
                color each point in scatter plot

    """
    def __init__(self, figure,axis, 
                dtypFlow,data,myAxes,myScat,
                nRowCol, myCol):
        self.fig = figure
        self.ax = axis
        self.dtypFlow = dtypFlow
        self.data = data
        self.nRowCol = nRowCol
        self.myAxes = myAxes
        self.myScat = myScat
        self.myCol = myCol

        #define rectangle
        self.rect = matplotlib.patches.Rectangle( (0,0), 0.0, 0.0, fill=True, alpha=0.5, color = "0.5")
        #enable button press/release and Key press
        self.cid = self.fig.canvas.mpl_connect('button_press_event', self.onClick)
        self.rid = self.fig.canvas.mpl_connect('button_release_event', self.onRel)
        self.kid = self.fig.canvas.mpl_connect('key_press_event', self.onKey)

    def onClick(self, event):
        print '%s, Event: ' % "Click", event   

        clickAx = event.inaxes
        if clickAx not in self.ax: return
        
        self.ix = event.x
        self.iy = event.y
        self.ixdata = event.xdata
        self.iydata = event.ydata

        #only enable motion here
        clickAx.add_patch(self.rect) ## add to axis clicked on
        mid = self.fig.canvas.mpl_connect('motion_notify_event', self.onMove)
        
    
    def onMove(self, event):
        print '%s, Event: ' % "Move", event
        
        if event.inaxes not in self.ax: return

        self.dx = event.xdata - self.ixdata
        self.dy = event.ydata - self.iydata

        #update rectangle
        self.rect.set_xy((self.ixdata,self.iydata))  #updated way do it from notebook
        self.rect.set_width(self.dx)
        self.rect.set_height(self.dy)
        self.fig.canvas.draw()

    def onRel(self, event):
        print '%s, Event: ' % "Release", event

        if event.inaxes not in self.ax: return

        self.dx = event.xdata - self.ixdata
        self.dy = event.ydata - self.iydata

        #final rectangle updata
        self.rect.set_xy((self.ixdata,self.iydata))  #updated way do it from notebook
        self.rect.set_width(self.dx)
        self.rect.set_height(self.dy)
        self.fig.canvas.draw()

        #fade out data OUTSIDE of rectangle
        xRng = [self.ixdata, self.ixdata + self.dx]
        yRng = [self.iydata, self.iydata + self.dy]
        my.rePlot(event.inaxes, xRng, yRng,
                  self.fig, self.ax, 
            self.dtypFlow,self.data,self.myAxes,self.myScat,
            self.nRowCol, self.myCol) 
        #re-Add rectangle as cleared axis
        event.inaxes.add_patch(self.rect)
        self.fig.canvas.draw()

    def onKey(self, event):
        print '%s, Event: ' % "Key", event
        sys.stdout.flush()
        key = event.key
        if key == "d":
            print "key is d"
        if key == "q":
            print "key is q"
        else:
            print "key is %s, which does nothing" % key


#Argparse
#Get file and pathToFile from Command Line
##################
arg1 = "path"
arg2 = "file"
parser = argparse.ArgumentParser(description='CalCalc parser')
parser.add_argument(arg1,
                    help='absolute path to data file, ending in "/" ',
                    default = "./")
parser.add_argument(arg2, 
                    help="data file to make subplots of",
                default = "flowers.csv")
usr = parser.parse_args()
print "%-20s= %s" % (arg1, usr.path)
print "%-20s= %s" % (arg2, usr.file)


#Read in data
##################
# fPath = "/Users/kburleigh/GradSchool/Fall_2013/python_ay250/python-seminar/Homeworks/hw_3_data/"
# fData = "flowers.csv"
fPath = usr.path
fData = usr.file

#
dtypFlow = {"names": ["lsep", "wsep", "lpet", "wpet","name"],
            "formats": [np.float64,np.float64,np.float64,np.float64, 'S10']
            }
data = np.loadtxt(fPath+fData, dtype=dtypFlow, delimiter=",", skiprows=1)

#Plot data
##################

myCol = data["name"]
myCol[myCol == "setosa"] = "blue" 
myCol[myCol == "versicolor"] = "red"
myCol[myCol == "virginica"] = "black"

myAxes = [] #1D list of axis objects, and scatter plot objects
myScat = []
nRowCol = len( dtypFlow["names"] ) - 1

#Big plotting loop for each subplot
##################
fig, ax = plt.subplots(nRowCol, nRowCol) #sharey=True, sharex=True)
for col in range(nRowCol):
    for row in range(nRowCol):
        xName = dtypFlow["names"][col]
        x = data[xName]
        yName = dtypFlow["names"][row]
        y = data[yName]
        myax = ax[row, col]      #[j,i] NOT [i,j] matches flower example
        myscat = myax.scatter(x,y, s=20, c=myCol)
        myax.set_xlim([x.min(),x.max()])
        myax.set_ylim([y.min(),y.max()])
        if xName == yName:
            myax.set_label( xName )
            myax.legend(loc=2)

        plt.setp(myax.get_xticklabels(), visible=False)  # num's under axis
        plt.setp(myax.get_yticklabels(), visible=False)
        myax.set_xlabel("",visible=False)  # title of axis
        myax.set_ylabel("",visible=False)
        if col == 0: 
            plt.setp( myax.get_yticklabels(), visible=True)
            myax.set_ylabel( dtypFlow["names"][row], visible=True)
        if row == nRowCol-1: 
            plt.setp( myax.get_xticklabels(), visible=True)
            myax.set_xlabel( dtypFlow["names"][col], visible=True)
        
        #save ax, scatter plot objects for later reference
        myAxes.append( myax )
        myScat.append( myscat )

#Turn on Interactive Rectangles
##################
RectMy = RectInteract(fig,ax, 
                      dtypFlow,data,myAxes,myScat,
                        nRowCol,myCol)
plt.show()


